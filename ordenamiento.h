// Se llaman las librerias a implementar
#include <iostream>

using namespace std;

#ifndef ORDEN_H
#define ORDEN_H

// Se define la clase del orden
class Orden {
	// Se definen atributos privados
    private:
    // Se definen atributos publicos
    public:
        // Se define el constructor
        Orden();
        // Se definen las funciones
        void imprimirDatosVector(int *arreglo, int numero_tamano); 		// Funcion que permite la impresion del arreglo
        void ordenarSelection(int *arreglo, string parametro, int numero_tamano);			// Funcion que permite el ordenamiento por Seleccion
        void ordenaQuicksort(int *arreglo, string parametro, int numero_tamano);		// Funcion que permite el ordenamiento por Quicksort
};
#endif
