# GUIA 8 METODOS DE ORDENAMIENTO
# Comenzando --- EXPLICACION PROBLEMATICA----
La problematica planteada en la octava guia del modulo trata de desarrollar por medio del lenguaje C++ la conversion de fragmentos de codigos entregados por el profesor en lenguaje C, los cuales permiten el funcionamiento de los metodos de ordenamiento, por medio de estos fragmentos y mediante el uso de clases se tiene que proceder a comparar el rendimiento de dos tipos como Seleccion y Quicksort entregando el valor del tiempo en cuanto se demora en ordenar los datos presentes. Es importante aclarar que los parametros de los datos tienen que ser positivos y menores o iguales a 1.000.000 los cuales se tendran que ir completando de manera aleatoria. El usuario a su vez puede ingresar si desea ver solamente el tiempo o si a su vez desea agregar ver el contenido de los datos. Se entregan como recursos la forma de realizar el aleatorio y los metodos de ordenamiento. 

# ---- COMO SE DESARROLLO ----
Para poder resolver la problematica propuesta en la guia lo primero que se realizo fue la comprension y la adaptacion del lenguaje C a C++, donde se comprendio que funciones realizaban especificamente cada linea del codigo y como permitian el ordenamiento, posteriormente se procedio a construir los archivos con sus clases respectivas y se llevo a cabo la construccion de las funciones que determinan el funcionamiento del codigo y permite su ejecucion correctamente. LOs archivos principales son detallados a continuacion con su funcionamiento y caracteristicas propias. 

- _Makefile:_  Permite que se pueda compilar el codigo mediante la terminal y desarrollar las pruebas de ejecucion en donde se indican los posibles errores del codigo y a la vez indica si la compilacion se ha desarrollado de manera exitosa. Es un requerimiento imprescindible dentro de los programas para poder ver su funcionalidad.

- _Ordenamiento.h_ : Este es el archivo cabecera del archivo Ordenamiento.cpp, contiene la estructura basica para que el código pueda funcionar y se genere la clase Orden la cual permite que pueda ser llamado en los distintos archivos de esta manera se puede desarrollar de manera global y contiene todas las estructuras necesarias con atributos privados y publicos, los atibutos de caracter publico son principalmente las funciones que permitiran del desarrollo del programa cada una tiene su propio tipo y tambien tiene una serie de atributos para usar en el desarrollo. Alguna de las funciones del archivo de cabecera son imprimirDatosVector la cual permite que se los datos del arreglo sean impresos, ordenarSelection esta funcion se encarga de poder ordenar los datos por medio de la tecnica de Seleccion, ordenaQuicksort esta funcion requiere el arreglo y el tamano las que luego se ordenan segun el metodo Quicksort.

- _Ordenamiento.cpp_ : En este archivo se llama al archivo cabecera el cual contiene las indicaciones de como deben funcionar las funciones, se llama al constructor de las funciones que en este caso de la clase ordenamiento del arreglo. Lo que se realiza en este archivo es poder llevar a cabo las validaciones de los metodos de ordenamiento y dos funciones primordiales que permiten la impresion de los datos del arreglo y tambien el tiempo en que los metodos se demoran en realizar. 
Luego se crea la funciones que permiten el funcionamiento del programa, las cuales son:

    - imprimirDatosVector: Esta funcion sirve para poder llevar a cabo la impresion de los datos del arreglo para esto se recorre y luego permite poder obtener los datos, se generan a su vez el diseño de la impresion con los guiones que separen los datos y a su vez se generan metodos para una mejor visualizacion de los datos para que esta no sea confunsa. Requiere los parametros del tamano y del arreglo.

     _Nota: esta funcion es basica para poder mostrar los elementos del arreglo de modo ordenado para conocer cuales son los datos del arreglo y como estos deben quedar de manera correcta, esta funcion se ejecuta cuando el usuario ingresa la opcion ver_

    - ordenarSelection: El metodo de seleccion es un proceso de orden interno con un metodo mas directo donde se reorganizan y reagrupan un conjunto de datos _ la finalidad es buscar un elemento menor y colocarlo en la primera posicion este proceso continua hasta que todos los n elementos esten ordenados_.Esta funcion se lleva a cabo gracias al ordenamiento de tipo seleccion en donde se encuentra el menor de los datos de todo el conjunto de elementos del arreglo el cual luego se intercambia con el que esta en la primera posicion, luego se ve el segundo mas pequeño hasta que ordena todo, su metodo de comparacion requiere intercambios de los valores para ordenar la secuencia de los elementos. Se define un atributo menor y auxiliar en donde se recorre el arreglo por medio de un ciclo for en donde se compara el valor menor con el arreglo y se igualan las posiciones, tambien se hace otro recorrido del arreglo pero con una posicion mas buscando el siguiente numero menor, si el dato del arreglo es menor al dato menor que se encuentre se igualan las posiciones y luego se van reemplazando y ordenando los datos. 
Tambien se agrega un parametro que este funcionara para poder ver la impresion de los datos por medio del ordemaniento para esto se llama a la funcion imprimirDatosVector de esta manera cuando el usuario ingrese la opcion "s" se podran ver los datos del arreglo ordenados por medio del metodo de Seleccion.

     _Nota: para poder realizar este ordenamiento se guio del codigo en c, el cual luego es transformado a c++ con las condiciones requeridas para su funcionamiento y para que ordene por medio del menor dato a su posicion correcta, se llama la funcion tiempo a su vez para poder realizar un algortimo mucho mas eficiente y que no se repita constantemente_

    - ordenaQuicksort: _En resumen este metodo permite poder recorrer una cadena desordenada de un numero n de elementos, es uno de los mas veloces y ordena por medio de la particion del arreglo_ este funcion se lleva a cabo cuando se toma un elemento cualquiera en un posicion desconocida (casi siempre se elige el primero), se trata de buscar el elemento en la posicion correcta, los numeros de la izquierda son menores o iguales al elemento n, a la derecha se ubican los mayores e iguales, luego se repite otra vez este preoceso pero con los que se encuentren a la izquierda y la derecha (fraccionamiento) este proceso finaliza cuando todos los datos estan ordenados en su posicion correcta. Esta funcion se lleva a cabo gracias al ordenamiento de tipo Quicksort es considerado uno de los mas rapidos y eficientes es necesario tener un array de elementos donde se separan los elementos por la izquierda y la derecha dependiendo si son mayores o menores los datos, por lo tanto el arrego es dividido en dos, luego se sigue subdiviendo hasta que los datos de forma recursiva se recorre de forma simultanea las listas que conforman el arreglo por la derecha y por la izquierda. Para el funcionamiento de esta funcion se definen atributos que veran la pila mayor la pila menor y tambien una serie de parametros que se ven desde el inicio, fin, posicion y luego por la derecha, izquierda y con un auxiliar. Es necesario definir una bandera para poder obtener si el dato cumplio o no por medio del True o false. Se genera una comparativa cuando el tope es menor o igual a 0 donde se separa al incio la pila menor y por el final la pila mayor, el tope luego decrece en un valor. El metodo es reducido y luego se hace una comparativa entre los elementos de la izquierda con el inicio, la derecha con el final y luego la posicion con el incio, la siguiente comparativa se realiza por medio de una bandera y si la posicion esta por la izquierda o derecha. Despues si el inicio es menor a la posicion que tiene un valor menos la pila menor sera igual al incio y la pila mayor es igual a la posicion menor a 1. Cuando el final es mayor a la posicion con un valor mas el tope aumente y la pila menor estara una posicion mas a la derecha y la pila mayor en el final. 

      Tambien se agrega un parametro que este funcionara para poder ver la impresion de los datos por medio del ordemaniento para esto se llama a la funcion imprimirDatosVector de esta manera cuando el usuario ingrese la opcion "s" se podran ver los datos del arreglo ordenados por medio del metodo de Quicksort.

      _Nota: para poder realizar este ordenamiento se guio del codigo en c, el cual luego es transformado a c++ con las condiciones requeridas para su funcionamiento por medio del ordenamiento de quicksort(donde se generan divisiones para establecer comparativas), se llama la funcion tiempo a su vez para poder realizar un algortimo mucho mas eficiente y que no se repita constantemente_

- _Programa.cpp_: En este archivo se llevan a cabo la funcionalidad del programa la cual puede ser probada y ademas se lleva a cabo la interaccion con el usuario. Se incluyen las librerías a implementar que permiten generar numeros de manera aleatorio (ctime, cstdlib) y también se llama al archivo que contiene las funciones para que se lleve a cabo el programa(cabecera). 

     En este archivo existen dos funciones principales la primera es la encargada de mostrarTiempo, esta funcion tiene un parametro que es el arreglo que se denomina con un nombre estandar de auxiliar y tambien el tamaño en esta funcion se puede mostrar la ineraccion que saldra al usuario con respecto al tiempo que se emplee en cada metodo, en esta funcion se requiere la clase Orden debido a que se necesitan llamar a las funciones que estan presentes en el archivo Ordenamiento.cpp porque son necesarias para el funcioamiento del programa, tambien dentro de esta funcion se calcula el tiempo de cada metodo de ordeamiento, se puedo realizar gracias a la busqueda en internet del funcionamiento de la libreria chronos y sus funciones la mas implementada y adecuada fue high_resolution_clock la cual es un relog que permite obtener el tiempo en disitntas unidades (en este caso en milisegundos) para poder hacer este calculo necesita el inicio y el final del relog para tener un intervalo de cuanto dura el desarrollo de la operatoria. dentro de esta misma funcion se imprime el metodo con el tiempo para que el usuario pueda visualizar y sacar sus propias conclusiones acerca de que metodo es mas eficiente y mas rapido.

     _Nota: La funcion high resolution clock como duracion tienen que ser implementadas luego de añadir la biblioteca chronos, si esta no se agrega el programa puede presentar fallas._

     Luego se define el main del programa con los parametros necesarios en donde estan los que simulan al arreglo y el que simula un valor entero, se declara el tamaño con una condicion que englobe el caracter numerico, tambien se define un arreglo y se llaman a la clase para poder ejecutar funciones del archivo Ordenamiento.cpp, tambien se define un parametro de tipo string (letras) para poder capturar la opcion del usuario. Ademas posterior de definir las variables y sus tipos junto con hacer otras validaciones se comienza a desarrollar el visualizador de los metodos de ordenamiento, para esto se llevan a cabo las condiciones del tamaño del numero que ingrese el usuario si este corresponde el arreglo se recorre y es llenado por numeros aleatorio, posteriormente cuando el usuario ingrese los parametros estos tienen que ser 3 si son mayores o menores estos no seran correctos y se mostrara un mensaje por la terminal. Las principales dos opciones para ver el funcioamiento del programa son los siguientes parametros.

     - _Parametro "s":_ se muestran los datos del arreglo de forma desordenada (como se generan aleatorios) para esto se llama la funcion imprimirDatos y el tiempo de cada uno de los metodos con los datos ordenados se muestran por medio de la funcion mostrarTiempo. 

     - _Parametro "n":_ De modo contrario si el usuario ingresa la opcion "n" solo se mostrara el tiempo de ejecucion de cada metodo sin los datos del arreglo ni desordenados ni ordenados.

     - Tambien si el usuario ingresa un parametro diferente se imprime un mensaje por terminal que es incorrecto y no corresponde. Finalmente si el usuario no ingresa un valor valido en el rango predeterminado tiene que ingresar nuevamente otro diferente. 

     _Nota: Se realizan las funciones de validaciones dentro del main para que no se generen errores o problematicas con el trato de los datos para poder ver el correcto funcionamiento de los ciclos y a su vez si se asignan las correctas funciones a cada parametro o atributo, a su vez tambien se puede observar que existen validaciones de los parametros como el tamaño de los datos que el usuario ingrese, cada uno presenta sus condiciones para poder llevar a cabo el funcionamiento y mostrar tanto el tiempo del metodo como los datos cuando esto sea requerido_

Finalmente podemos decir que el funcionamiento del programa es correcto y logra resolver la problematica propuesta de la octava guia (tema6), ademas se realizan de manera correcta las validaciones cuando el usuario ingresa un numero que no esta entre el rango de 0 a 1.000.000, cuando ingresa mas de tres argumentos tambien se realiza una validacion y si el parametro de ver ingresado no es una S o N se imprime un mensaje para el usuario. Tambien permite poder imprimir los datos del arreglo que se generan de manera aleatorio cuando el usuario ingresa la opcion s, tambien se puede independiente de la ocpion del usuario siempre se ven los tiempos (en milisegundos) en los cuales se demoran en ordenar los datos que el usuario ingresa sin mostrarlos de manera directa por meido de la opcion n. El programa no presenta errores en su funcionamiento, y cuando se ingresan parametros desconocidos o que no corresponden automaticamente no se ejecuta una opcion, y cuando se ingresan parametros validos este funciona de manera optima cumpliendo con la resolucion de la problematica mediante el uso del codigo entregado y traspasado de C a C++. 

# Prerequisitos
- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)

# Instalacion
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:

- _lsb_release -a (versión de Ubuntu)_

En donde: Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando. Para descargar un editor de texto como vim se puede descargar de la siguiente manera:

- _sudo apt install vim _

En donde: Por medio de este editor de texto se puede construir el codigo. En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.
Tambien si se desea ocupar un editor de texto diferente a vim este se puede instalar por la terminal o por el centro de software en donde se escribe el editor que se desea conseguir y luego se ejecuta la descarga, de esta manera se pudo descargar el editor que se ocupa para la realizacion de esta guia el cual es atom, se eligio a la vez este editor debido a que permite una interfaz mejorada con respecto a la compilacion y a su vez permite que se revisen y compilen los archivos de la terminal disminuyendo posibles errores.

## Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa
Para poder ejecutar el programa se tiene que hacer las siguientes condiciones:

Se debe ingresar a la carpeta donde se esta trabajando con los archivos del programa y esta debe contener el archivo Makefile, si este no ha sido probado con anterioridad luego se agrega el comando:

- _make_ 

De esta manera se esta compilando el trabajo, si este no presenta comentarios significa que la prueba finalizo con exito.

Por otra parte si se esta probando con el comando G++ se tiene que hacer el siguiente comando por terminal:
- _g++ programa.cpp ordenamiento.cpp_

Si la carpeta con archivos presenta otras extensiones diferentes a .cpp, .h o al archivo makefile se debe ejecutar el siguiente comando:

- _make clean_ 

Posteriormete se ejecuta en la terminal

- _make_.

Luego se lleva a cabo el programa ejecutando el siguiente comando por terminal:
- _./programa numero parametro_

En donde numero hace referencia al valor numerico que ingrese el usuario que tiene que se positivo y menor o igual a 1.000.000, si el numero ingresado no es correcto se arrojaran una validaciones y mensajes al usuario.

Tambien parametro hace referencia a la opcion que el usuario debe ingresar la cual esta determinada con las letras "s" y "n".
Cuando el usuario ingrese el parametro "s": Se veran todos los datos generados aleatorios, luego los datos ordenados de cada metodo y finalmente el tiempo de ejecucion. Se tendria que ingresar de esta manera con el comando:

- _./programa numero s_

Si se ingresa la opcion "n": Se vera solo el tiempo de ejecucion de los metodos sin la impresion de niguno de los vectores del arreglo. Se tendria que ingresar de esta manera con el comando:

- _./programa numero n_

Tambien si se agrega un parametro diferente de 3 argumentos o una letra distinta se enviara un mensaje de error al usuario. Unos ejemplos de estos errores son:

- _./programa numero parametro parametro parametro (mas de 3 parametros o menos)_
- _./programa numero parametro (distinto de s o n)_

## INSTALACION MAKEFILE
Para poder llevar a cabo el funcionamiento del programa se requiere la instalacion del Make para esto primero hay que revisar que este en la computadora, para corroborar se debe ejecutar este comando:
- _make_

De esta forma se puede ver si esta instalado para luego llevar a cabo el desarrollo del programa, si este no esta instalado, se tiene que insertar el siguiente comando en la terminal de la carpeta del programa.

- _sudo apt install make_

De esta manera se procede a la creacion del make para que luego este se lleve a cabo y se pueda observar la construccion del archivo.
Luego de instalar el Make, para poder ejecutar las pruebas se tiene que en la terminal entrar en la carpeta donde se encuentran los archivos mediante el comando cd con el nombre de dicha carpeta, luego ver si estan los archivos correctos o vizualizar lo que contiene la carpeta se debe ingresar el comando ls, finalmente se debe escribir make en la terminal.

Si ya se ha ejecutado con anterioridad un make, se tiene que escribir un make clean para borrar los archivos probados con anterioridad. Luego de ejecutar en la terminal el comando:
- _make_

Debe mostrar los nombres de los archivos y el orden de estos con g++ y el nombre del archivo, aqui se puede visualizar si el programa no tiene problematicas. 
Si hay una problematica sale un mensaje y no salen todos los archivos con g++. Si se desarrollo completamente luego se puede ingresar el siguiente comando:
- _./ con el nombre del programa el cual contiene el main._

De esta manera se observa el funcionamiento del programa y de los demas archivos.

# Construido con
- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Atom: Editor de código.

# Versiones
## Versiones de herramientas:
- Ubuntu 20.04 LTS
- Atom 1.52.0
- Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/guia8-aed

# Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

# Expresiones de gratitud
- A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=731116

- A las Lecturas de internet y videos que me ayudaron a plantear una solucion posible: 
https://www.youtube.com/watch?v=hlC1H8U0WXk
https://www.youtube.com/watch?v=oEx5vGNFrLk
https://www.geeksforgeeks.org/chrono-in-c/
http://www.cplusplus.com/reference/chrono/high_resolution_clock/now/
https://blog.martincruz.me/2012/09/obtener-numeros-aleatorios-en-c-rand.html
https://www.programarya.com/Cursos/C++/Funciones
http://www.it.uc3m.es/pbasanta/asng/course_notes/functions_passing_parameters_es.html
https://translate.google.com/translate?hl=es&sl=en&u=http://www.cplusplus.com/reference/chrono/high_resolution_clock/now/&prev=search&pto=aue

