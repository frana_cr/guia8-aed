// Se llaman las liberias a implementar
#include <iostream>
#include <cstdlib> // libreria del aleatorio
#include <ctime> // libreria del aleatorio
#include <fstream>
#include <iostream>
#include <chrono> // libreria del tiempo

using namespace std;

// Se llama al archivo de cabecera que contiene informacion de las funciones
#include "ordenamiento.h"

// Se crea la funcion que permite poder mostrar el tiempo en base a cada metodo
void mostrarTiempo(int *aux1, string parametro, int tamano){
  Orden orden; 				// Se llama a la clase y se le otorga otro nombre
  // DE ESTA MANERA EL USUARIO VERA EL TIEMPO SEGUN EL METODO
  cout << "\n-----------------------------------------" << endl;
  cout << "   METODO Y TIEMPO" << endl;     // Se imprime de esta manera un mensaje al usuario
  cout << "-----------------------------------------" << endl;

  // SE CALCULA EL TIEMPO DEL METODO SELECCION
  auto inicio1 = chrono::high_resolution_clock::now(); 		// Se genera un inicio del relog del tiempo
  // Se llama a la funcion que ordena la que permite la impresion de los datos ordenados, del arreglo con el tamaño
  orden.ordenarSelection(aux1, parametro, tamano);
  auto fin1 = chrono::high_resolution_clock::now();		// Se establece un final del relog del tiempo
  chrono::duration<float,std::milli> duration_selection = (fin1 - inicio1); // se imprime la duracion del metodo restando el tiempo final e inicial
  cout << "Metodo : SELECTION |" << " Tiempo: " << duration_selection.count() << " Milisegundos " << std::endl; // La duracion se imprime en milisegundos y con decimales
  cout << "-----------------------------------------" << endl;

    // SE CALCULA EL TIEMPO DEL METODO QUICKSORT
  auto inicio2 = chrono::high_resolution_clock::now(); 		// Se genera un inicio del relog del tiempo
  // Se llama a la funcion que ordena la que permite la impresion de los datos ordenados, del arreglo con el tamaño
  orden.ordenaQuicksort(aux1, parametro, tamano);
  auto fin2 = chrono::high_resolution_clock::now();		// Se establece un final del relog del tiempo
  chrono::duration<float,std::milli> duration_quicksort = (fin2 - inicio2); // se imprime la duracion del metodo restando el tiempo final e inicial
  cout << "Metodo: QUICKSORT |" << " Tiempo: " << duration_quicksort.count() << " Milisegundos " << std::endl; // La duracion se imprime en milisegundos y con decimales
  cout << "-----------------------------------------" << endl;
}

// Se llama a la funcion main (con los parametros necesarios)
int main(int argc, char *argv[]){
	// Se definen las variables con su tipo
  int tamano;
  tamano = atoi(argv[1]);		// La funcion tamaño se transforma para poder trabajar con ella (convierte de cadena a numero)
  int arreglo[tamano];			// Se crea el arreglo que contiene el tamano
  Orden orden;					// Se llama a la clase y se le otorga otro nombre
  int numero_random;			// Se define la variable que contendra el numero aleatorio
  string parametro_ver =  argv[2]; // Se define un parametro de tipo string para almacenar la opcion del usuario

	// Se comienza a desarrollar el visualizador de los metodos
  cout << " BIENVENIDO AL VISUALIZADOR DE METODOS DE ORDENAMIENTOS " << endl;
  // Si el tamano del numero ingresado es mayor a 0 y menor o igual a 1.000.000 se continua
  if (tamano > 0 && tamano <= 1000000){
	  // Se recorre el arreglo
    //srand((unsigned) time(0));
    for (int i = 0; i < tamano; i++) {
		// Se genera el numero random y es almacenado
      numero_random = (rand() % tamano);
      // El arreglo contendra el numero random
      arreglo[i] = numero_random;
    }
    if (argc == 3){			// ESTA CONDICION PERMITE VERIFICAR SI SE INGRESAN solo 3 parametros(normal)
      if (parametro_ver == "s"){ // Si se inserta la letra s, se muestran los datos del arreglo y el tiempo
        cout << "\nLos datos del arreglo DESORDENADOS son: " << endl; // se imprimen los datos desordenados
        orden.imprimirDatosVector(arreglo, tamano); 	// Funcion que imprime los datos
        mostrarTiempo(arreglo, parametro_ver, tamano);			// Funcion encargada de ver el tiempo
      }
      else if(parametro_ver == "n"){ // Si se inserta la letra n se muestra solo el tiempo de los metodos
        mostrarTiempo(arreglo,parametro_ver, tamano);			// Funcion encargada de ver mostrar el tiempo
      }
      else{ 			// Si no se ingresa ninguna opcion correcta se imprime el mensaje al usuario
        cout << " El parametro VER ingresado es INCORRECTO" << endl;
        cout << "Sugerencia los parametos validos son minusculas y s:Ver datos y tiempo , n: Ver solo tiempo" << endl;
      }
    }
    else {			// Si la cantidad de datos ingresados son distintos a 3 la entrada del parametro es invalida
      cout << "El valor de entrada del parametro es INVALIDA, intenta nuevamente" << endl;
    }
  }
  else{		// Si el dato ingresado no cumple las condiciones es invalido
    cout << "El dato ingresado NO ES VALIDO, intenta nuevamente" << endl;
  }
  return 0;
}